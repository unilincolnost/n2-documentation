---
layout: object

title: Location Nickname
nav: objects
endpoint: /location_nicknames

query_params :
 - name: location_id
   type: int
   description: Returns nicknames associated with the specified location. Uses the Nuclues ID of the location.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130129","pagination":{"page_size":50,"items_on_page":0,"current_page":0,"current_row":0,"total_rows":0,"last_row":0,"total_pages":0,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":false,"next_page":0,"next_row":0},"results":[]}'

additional_endpoints:
 - url: /id/<id>
   description: Returns a specific infrastructure nickname record, when providing a valid ID.
---
