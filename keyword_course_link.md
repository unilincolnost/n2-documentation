---
layout: object

title: Keyword Course Link
nav: objects
endpoint: /keyword_course_links

query_params :
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.
 - name: keyword_id
   type: int
   description: Returns links relevant to the specified keyword.
 - name: course_code_id
   type: int
   description: Returns links relevant to the specified course code.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130123","pagination":{"page_size":1,"items_on_page":1,"current_page":1,"current_row":0,"total_rows":6227,"last_row":6227,"total_pages":6227,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":true,"next_page":2,"next_row":1},"results":[{"id":1,"nucleus_url":"http:\/\/n2\/keyword_course_links\/id\/1","keyword_id":"82","course_code_id":"57","count":null,"relevance":"1"}]}'

additional_endpoints:
 - url: /id/
   description: Returns a specific record for a keyword course link, when providing a valid ID.
---

Links between course codes and keywords.