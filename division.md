---
layout: object

title: Division
nav: objects
endpoint: /divisions

query_params :
 - name: name
   type: string
   description: The name given to a Division at the university.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130125","pagination":{"page_size":1,"items_on_page":1,"current_page":1,"current_row":0,"total_rows":36,"last_row":36,"total_pages":36,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":true,"next_page":2,"next_row":1},"results":[{"id":1,"nucleus_url":"http:\/\/n2\/divisions\/id\/1","name":"Library"}]}'

additional_endpoints:
 - url: /id/<id>
   description: Returns a specific record for a division, when providing a valid ID.
---

Divisions exist within the organisational hierarchy of the university.