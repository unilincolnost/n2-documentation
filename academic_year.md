---
layout: object

title: Academic Year
nav: objects
endpoint: /academic_years

query_params :
 - name: name
   type: string
   description: Returns academic years with a 'name' like the one specfied. i.e name=12 will return 2011-12 and 2012-13.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json : '{"error":false,"http_status_code":200,"nucleus_version":"2.20130123","pagination":{"page_size":1,"items_on_page":1,"current_page":1,"current_row":0,"total_rows":9,"last_row":9,"total_pages":9,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":true,"next_page":2,"next_row":1},"results":[{"id":1,"nucleus_url":"http:\/\/n2\/academic_years\/id\/1","name":"2011-12"}]}'

additional_endpoints:
 - url: /id/<id>
   description: Returns a specific record for an academic year, when providing a valid ID.
---