---
layout: object

title: Meter Type
nav: objects
endpoint: /meter_types

query_params :
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.  Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.   Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130125","pagination":{"page_size":1,"items_on_page":1,"current_page":1,"current_row":0,"total_rows":1,"last_row":1,"total_pages":1,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":false,"next_page":1,"next_row":1},"results":[{"id":1,"nucleus_url":"http:\/\/n2\/meter_types\/id\/1","name":"Electricity Meter","unit":"kWh","cumulative":"0"}]}'

additional_endpoints:
 - url: /id/
   description: Returns a specific record for a meter type, when providing a valid ID.
---

Types of energy meters at the university.