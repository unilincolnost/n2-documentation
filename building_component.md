---
layout: object

title: Building Component
nav: objects
endpoint: /building_components

query_params :
 - name: building_id
   type: int
   description: The Nucleus ID of the building to which the components belong.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130129","pagination":{"page_size":1,"items_on_page":1,"current_page":1,"current_row":0,"total_rows":237,"last_row":237,"total_pages":237,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":true,"next_page":2,"next_row":1},"results":[{"id":81,"nucleus_url":"https:\/\/n2.online.lincoln.ac.uk\/building_components\/id\/81","planon_id":618,"building":{"id":12,"uri":"id.lincoln.ac.uk\/building\/12","nucleus_url":"https:\/\/n2.online.lincoln.ac.uk\/buildings\/id\/12","osm_way_id":195112298,"planon_id":5000729,"estates_code":"BR-018","name":"Court 01","build_year":1998,"latitude":53.230661,"longitude":-0.55595,"maki_icon":"lodging"},"code":0,"name":"Ground Floor","internal_area":450.966,"external_area":489.33}]}'

additional_endpoints:
 - url: /id/<id>
   description: Returns a specific record for a building component, when providing a valid ID.
---
