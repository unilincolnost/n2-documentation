---
layout: object

title: Meter
nav: objects
endpoint: /meters

query_params :
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130125","pagination":{"page_size":1,"items_on_page":1,"current_page":1,"current_row":0,"total_rows":33,"last_row":33,"total_pages":33,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":true,"next_page":2,"next_row":1},"results":[{"id":4,"nucleus_url":"http:\/\/n2\/meters\/id\/4","name":"Architecture Building Architecture E M (kWh)","description":null,"meter_type_id":"1","valid_for":"0"}]}'

additional_endpoints:
 - url: /id/
   description: Returns a specific record for a meter, when providing a valid ID.
---

Energy meters at the university.