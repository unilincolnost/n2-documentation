---
layout: object

title: Terrain
nav: objects
endpoint: /terrains

query_params :
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":2,"num_results":1,"results":[{"id":1,"planon_id":"5000890","estates_code":"BR-002","name":"Southwest Quadrant","build_year":null,"location_id":"3"}]}'

additional_endpoints:
 - url: /id/<id>
   description: Returns a specific terrain record, when providing a valid ID.
---
