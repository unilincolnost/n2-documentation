---
layout: object

title: Institution
nav: objects
endpoint: /institutions

query_params :
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

additional_endpoints:
 - url: /id/
   description: Returns a specific record for an institution, when providing a valid ID.
---
