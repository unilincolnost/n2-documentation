---
layout: object

title: Faculty
nav: objects
endpoint: /faculties

query_params :
 - name: college_id
   type: int
   description: Returns faculties with the specified parent college.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130129","pagination":{"page_size":1,"items_on_page":1,"current_page":1,"current_row":0,"total_rows":9,"last_row":9,"total_pages":9,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":true,"next_page":2,"next_row":1},"results":[{"id":1,"nucleus_url":"https:\/\/n2.online.lincoln.ac.uk\/faculties\/id\/1","code":"CERDFAC","title":"Centre for Educational Research and Development","college":{"id":4,"uri":"id.lincoln.ac.uk\/college\/4","nucleus_url":"https:\/\/n2.online.lincoln.ac.uk\/colleges\/id\/4","code":"COSS","title":"College of Social Sciences","institution":{"id":1,"uri":"id.lincoln.ac.uk\/instituion\/1","nucleus_url":"https:\/\/n2.online.lincoln.ac.uk\/institutions\/id\/1","name":"University of Lincoln"}}}]}'

additional_endpoints:
 - url: /id/
   description: Returns a specific record for a faculty, when providing a valid ID.
 - url: /code/
   description: Returns a specific record for a faculty, when providing a valid faculty code.
---
