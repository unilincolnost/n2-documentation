---
layout: object

title: Accrediting Body
nav: objects
endpoint: /accrediting_bodies

query_params :
 - name: royal_charter
   type: bool
   description: Indicates if the accrediting body has a royal charter.
 - name: statutory_body
   type: bool
   description: Indicates if the accrediting body is a statutory authority.
 - name: name
   type: string
   description: The name of the accrediting body.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.  Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.   Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130123","pagination":{"page_size":1,"items_on_page":1,"current_page":1,"current_row":0,"total_rows":162,"last_row":162,"total_pages":162,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":true,"next_page":2,"next_row":1},"results":[{"id":1,"nucleus_url":"http:\/\/n2\/accrediting_bodies\/id\/1","name":"American Veterinary Medical Association (AVMA)","url":"http:\/\/www.avma.org\/default.asp","royal_charter":false,"statutory_body":false}]}'

additional_endpoints:
 - url: /id/<id>
   description: Returns a specific record for an accrediting body, when providing a valid ID.
---
