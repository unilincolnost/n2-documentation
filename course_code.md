---
layout: object

title: Course Code
nav: objects
endpoint: /contact_codes

query_params :
 - name: code
   type: string
   description: Search for course codes 'like' the string specified.
 - name: keywords
   type: int
   description: A comma-seperated list of keyword ids. Returns course codes associated with these keywords.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.  Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.   Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130123","pagination":{"page_size":1,"items_on_page":1,"current_page":2,"current_row":1,"total_rows":373,"last_row":373,"total_pages":373,"has_previous":true,"previous_page":1,"previous_row":0,"has_next":true,"next_page":3,"next_row":2},"results":[{"id":2,"nucleus_url":"http:\/\/n2\/course_codes\/id\/2","code":"CAHTPAUI"}]}'

additional_endpoints:
 - url: /id/
   description: Returns a specific record for a course code, when providing a valid ID.
 - url: /code/
   description: Returns a specific record for a course code, when providing a valid code.
---
