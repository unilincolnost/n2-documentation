---
layout: object

title: Meter Reading
nav: objects
endpoint: /meter_readings

query_params :
 - name: meter_id
   type: int
   description: Returns meter readings for a specific meter. Uses the ID field of the meter.
 - name: from
   type: int
   description: Returns meter readings after the specified date / time.
 - name: to
   type: int
   description: Returns meter readings before the specified date / time.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.  Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.   Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130125","pagination":{"page_size":1,"items_on_page":1,"current_page":1,"current_row":0,"total_rows":833216,"last_row":833216,"total_pages":833216,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":true,"next_page":2,"next_row":1},"results":[{"id":1,"nucleus_url":"http:\/\/n2\/meter_readings\/id\/1","timestamp":"2011-05-08 00:00:00","meter_id":"4","value":"0"}]}'

additional_endpoints:
 - url: /id/
   description: Returns a specific record for a meter reading, when providing a valid ID.
---

Types of energy meters at the university.