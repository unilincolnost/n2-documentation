---
layout: object

title: Benchmark Statement Level
nav: objects
endpoint: /benchmark_statement_levels

query_params :
 - name: level
   type: string
   description: The textual description of the benchmark statement level.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json : '{"error":false,"http_status_code":200,"nucleus_version":"2.20130123","pagination":{"page_size":1,"items_on_page":1,"current_page":1,"current_row":0,"total_rows":9,"last_row":9,"total_pages":9,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":true,"next_page":2,"next_row":1},"results":[{"id":1,"nucleus_url":"http:\/\/n2\/benchmark_statement_levels\/id\/1","level":"1"}]}'

additional_endpoints:
 - url: /id/<id>
   description: Returns a specific record for a benchmark statement level, when providing a valid ID.
---