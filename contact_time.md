---
layout: object

title: Contact Time
nav: objects
endpoint: /contact_times

query_params :
 - name: type_id
   type: int
   description: The type of contact time i.e. lecture, seminar. Uses the Nucleus ID for contact type.
 - name: hours
   type: int
   description: The amount of hours spent on a given contact type for a module of study.
 - name: hours_gt
   type: int
   description: Returns instances where the amount of time is greater than the amount specified.
 - name: hours_gte
   type: int
   description: Returns instances where the amount of time is greater than or equal to the amount specified.
 - name: hours_lt
   type: int
   description: Returns instances where the amount of time is less than the amount specified.
 - name: hours_lte
   type: int
   description: Returns instances where the amount of time is less than or equal to the amount specified.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.  Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.   Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130123","pagination":{"page_size":1,"items_on_page":1,"current_page":2,"current_row":1,"total_rows":2635,"last_row":2635,"total_pages":2635,"has_previous":true,"previous_page":1,"previous_row":0,"has_next":true,"next_page":3,"next_row":2},"results":[{"id":2,"nucleus_url":"http:\/\/n2\/contact_times\/id\/2","hours":2,"module":{"id":110,"nucleus_url":"http:\/\/n2\/modules\/id\/110","module_code":{"id":33,"nucleus_url":"http:\/\/n2\/module_codes\/id\/33","code":"ISG2003M"},"title":"Advanced Digital Practice 2009-10","level":{"id":2,"nucleus_url":"http:\/\/n2\/levels\/id\/2","description":"Level 2"}},"contact_type":{"id":12,"nucleus_url":"http:\/\/n2\/contact_types\/id\/12","title":"Seminar","contact_type_category":{"id":3,"nucleus_url":"http:\/\/n2\/contact_type_categories\/id\/3","title":"Scheduled"}}}]}'

additional_endpoints:
 - url: /id/<id>
   description: Returns a specific contact time record, when providing a valid ID.
---