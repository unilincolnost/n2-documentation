---
layout: object

title: College
nav: objects
endpoint: /colleges

query_params :
 - name: title
   type: string
   description: The name given to a College at the university.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130129","pagination":{"page_size":1,"items_on_page":1,"current_page":1,"current_row":0,"total_rows":3,"last_row":3,"total_pages":3,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":true,"next_page":2,"next_row":1},"results":[{"id":2,"uri":"id.lincoln.ac.uk\/college\/2","nucleus_url":"https:\/\/n2.online.lincoln.ac.uk\/colleges\/id\/2","code":"COA","title":"College of Arts","institution":{"id":1,"uri":"id.lincoln.ac.uk\/instituion\/1","nucleus_url":"https:\/\/n2.online.lincoln.ac.uk\/institutions\/id\/1","name":"University of Lincoln"}}]}'

additional_endpoints:
 - url: /id/<id>
   description: Returns a specific record for a college, when providing a valid ID.
 - url: /code/<code>
   description: Reutnrs a specific record for a college, when providing a valid college code.
---

Colleges exist within the organisational hierarchy of the university.