---
layout: object

title: JACS Code
nav: objects
endpoint: /jacs_codes

query_params :
 - name: title
   type: string
   description: The title given to the JACS code.
 - name: code_like
   type: string
   description: Search for JACS codes 'like' the one specified.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.  Default 100.
 - name: pagex
   type: int
   description: Allows you to set which page of results to return.   Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130123","pagination":{"page_size":1,"items_on_page":1,"current_page":1,"current_row":0,"total_rows":1572,"last_row":1572,"total_pages":1572,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":true,"next_page":2,"next_row":1},"results":[{"id":1,"nucleus_url":"http:\/\/n2\/jacs_codes\/id\/1","code":"A100","title":"Pre-clinical medicine"}]}'

additional_endpoints:
 - url: /id/
   description: Returns a specific record for a JACS code, when providing a valid ID.
 - url: /code/
   description: Returns a specific record for a JACS code, when providing a valid code.
---

JACS codes are used to identify topic areas.
