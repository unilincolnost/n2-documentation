---
layout: object

title: Programme Outcome Category
nav: objects
endpoint: /programme_outcome_categories

query_params :
 - name: title
   type: string
   description: The title given to the category of programme outcomes.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130123","pagination":{"page_size":1,"items_on_page":1,"current_page":1,"current_row":0,"total_rows":4,"last_row":4,"total_pages":4,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":true,"next_page":2,"next_row":1},"results":[{"id":1,"nucleus_url":"http:\/\/n2\/programme_outcome_categories\/id\/1","title":"Knowledge and Understanding"}]}'

additional_endpoints:
 - url: /id/
   description: Returns a specific record for a programme outcome category, when providing a valid ID.
---

Programme outcomes are divided into categories.
