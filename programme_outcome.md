---
layout: object

title: Programme Outcome
nav: objects
endpoint: /programme_outcomes

query_params :
 - name: outcome
   type: string
   description: The text description of the programme outcome.
 - name: sandwich
   type: bool
   description: Denotes if the programme outcome only applies to the sandwich variant of the programme, where applicable.
 - name: programme_id
   type: id
   description: The ID of the programme to which the programme outcome applies.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.  Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.   Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130123","pagination":{"page_size":1,"items_on_page":1,"current_page":1,"current_row":0,"total_rows":26350,"last_row":26350,"total_pages":26350,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":true,"next_page":2,"next_row":1},"results":[{"id":1,"nucleus_url":"http:\/\/n2\/programme_outcomes\/id\/1","category":{"id":1,"nucleus_url":"http:\/\/n2\/programme_outcome_categories\/id\/1","title":"Knowledge and Understanding"},"description":"Have a knowledge and understanding of a wide range of contemporary literary texts in different genres and from different cultural traditions, and especially of their distinctive formal characteristics and thematic concerns.","sandwich_variant_only":false}]}'

additional_endpoints:
 - url: /id/
   description: Returns a specific record for a programme outcome, when providing a valid ID.
---
