---
layout: cwd

title: Security
nav: security
---

<div class="row">
<div class="span8">

Nucleus will require token based authentication.

Nucleus is *only* available over HTTPS connections.

</div>

<div class="span4">
<div class="well">

### Applications

In order to obtain authorisation tokens and use Nucleus you must first have a registered application.

</div>
</div>
</div>