---
layout: object

title: Space
nav: objects
endpoint: /spaces

query_params :
 - name: building_component_id
   type: int
   description: Returns spaces related to the specified building component.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":2,"num_results":1,"results":[{"id":1,"building_component_id":"1","planon_id":"8358","space_code":"VH0006","space_name":"Seminar Room","internal_area":"28.106","type_code":"35","type_name":"Seminar room - chairs with tablets","trac_code":"T","ems_code":"D12.C03","ems_name":"Teaching - other","tariff_code":"C_1112"}]}'

additional_endpoints:
 - url: /id/<id>
   description: Returns a space record, when providing a valid ID.
---
