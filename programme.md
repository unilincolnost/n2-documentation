---
layout: object

title: Programme
nav: objects
endpoint: /programmes

query_params :
 - name: academic_year
   type: int
   description: The ID of the academic year.
 - name: award
   type: int
   description: The ID of the award that programmes are associated with.
 - name: course_code_id
   type: int
   description: The ID of the course_code associated with the programme.
 - name: ucas_course_code
   type: string
   description: The UCAS course code associated with programmes.
 - name: ucas_programme_code
   type: string
   description: The UCAS programme code associated with programmes.
 - name: placement
   type: bool
   description: Indicates if a programme includes a placement or exchange.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.  Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.  Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":2,"num_results":1,"results":[{"id":1,"nucleus_uri":"http:\/\/n2\/programmes\/id\/1","programme_title":"21st Century Literature","course_title":"21st Century Literature Master of Arts (MA) 2011-12","url":null,"academic_year":{"id":1,"nucleus_uri":"http:\/\/n2\/academic_years\/id\/1","name":"2011-12"},"institution":{"id":1,"nucleus_uri":"http:\/\/n2\/institutions\/id\/1","name":"University of Lincoln"},"award":{"id":34,"nucleus_uri":"http:\/\/n2\/awards\/id\/34","title":"Master of Arts (MA)"},"course_code":{"id":1,"nucleus_uri":"http:\/\/n2\/course_codes\/id\/1","code":"CNTLTRMA"}}]}'

additional_endpoints:
 - url: /id/
   description: Returns a specific record for a programme, when providing a valid ID.
 - url: /course_code/
   description: Returns the latest programme of study related to the course code specified.
---

Programmes represent individual programmes of study towards a specific award.
