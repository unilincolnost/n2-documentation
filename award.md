---
layout: object

title: Award
nav: objects
endpoint: /awards

query_params :
 - name: title
   type: string
   description: The title of an award.
 - name: abb_title
   type: string
   description: The abbreviated title given to an award, i.e. PhD, BSc.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json : '{"error":false,"http_status_code":200,"nucleus_version":"2.20130123","pagination":{"page_size":1,"items_on_page":1,"current_page":1,"current_row":0,"total_rows":56,"last_row":56,"total_pages":56,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":true,"next_page":2,"next_row":1},"results":[{"id":1,"nucleus_url":"http:\/\/n2\/awards\/id\/1","title":"Bachelor of Architecture (BArch)"}]}'

additional_endpoints:
 - url: /id/<id>
   description: Returns a specific record for an award, when providing a valid ID.
---

Award

