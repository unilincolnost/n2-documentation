---
layout: object

title: Terrain Nickname
nav: objects
endpoint: /terrain_nicknames

query_params :
 - name: terrain_id
   type: int
   description: Returns nicknames for the specified terrain. Uses the Nucleus ID of the terrain.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":2,"num_results":1,"results":[{"id":1,"terrain_id":"4","nickname":"Estates Car Park"}]}'

additional_endpoints:
 - url: /id/<id>
   description: Returns a specific terrain record, when providing a valid ID.
---
