---
layout: object

title: Subject
nav: objects
endpoint: /subjects

query_params :
 - name: jacs_code_id
   type: id
   description: The ID of a JACS code.
 - name: school_id
   type: int
   description: The ID of a school, returns subjects taught by the school.
 - name: title
   type: string
   description: Returns subjects with a title 'like' the string specified.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130123","pagination":{"page_size":1,"items_on_page":1,"current_page":1,"current_row":0,"total_rows":69,"last_row":69,"total_pages":69,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":true,"next_page":2,"next_row":1},"results":[{"id":1,"nucleus_url":"http:\/\/n2\/subjects\/id\/1","title":"Human Nutrition","jacs_code":{"id":40,"nucleus_url":"http:\/\/n2\/jacs_codes\/id\/40","code":"B400","title":"Nutrition"},"school":{"id":21,"uri":"id.lincoln.ac.uk\/school\/21","nucleus_url":"http:\/\/n2\/schools\/id\/21","code":"LIFS","title":"School Of Life Sciences","faculty":{"id":8,"nucleus_url":"http:\/\/n2\/faculties\/id\/8","code":"SCI","title":"Faculty of Science","college":{"id":3,"uri":"id.lincoln.ac.uk\/college\/3","nucleus_url":"http:\/\/n2\/colleges\/id\/3","code":"COS","title":"College of Science","institution":{"id":1,"uri":"id.lincoln.ac.uk\/instituion\/1","nucleus_url":"http:\/\/n2\/institutions\/id\/1","name":"University of Lincoln"}}}}}]}'

additional_endpoints:
 - url: /id/
   description: Returns a specific record for a subject, when providing a valid ID.

---

Subjects relate to JACS codes and schools within the univeristy.
