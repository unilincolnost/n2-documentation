---
layout: object

title: School
nav: objects
endpoint: /schools

query_params :
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130123","pagination":{"page_size":1,"items_on_page":1,"current_page":1,"current_row":0,"total_rows":25,"last_row":25,"total_pages":25,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":true,"next_page":2,"next_row":1},"results":[{"id":1,"uri":"id.lincoln.ac.uk\/school\/1","nucleus_url":"http:\/\/n2\/schools\/id\/1","code":"CERD","title":"Centre For Educational Research And Development","faculty":{"id":1,"nucleus_url":"http:\/\/n2\/faculties\/id\/1","code":"CERDFAC","title":"Centre for Educational Research and Development","college":{"id":4,"uri":"id.lincoln.ac.uk\/college\/4","nucleus_url":"http:\/\/n2\/colleges\/id\/4","code":"COSS","title":"College of Social Sciences","institution":{"id":1,"uri":"id.lincoln.ac.uk\/instituion\/1","nucleus_url":"http:\/\/n2\/institutions\/id\/1","name":"University of Lincoln"}}}}]}'

additional_endpoints:
 - url: /id/
   description: Returns a specific record for a school, when providing a valid ID.
 - url: /code/
   description: Returns a specific record for a school, when providing a valid code.
---

Schools form part of the Univerity's organisational structure.
