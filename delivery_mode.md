---
layout: object

title: Delivery Mode
nav: objects
endpoint: /delivery_modes

query_params :
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130123","pagination":{"page_size":1,"items_on_page":1,"current_page":2,"current_row":1,"total_rows":5,"last_row":5,"total_pages":5,"has_previous":true,"previous_page":1,"previous_row":0,"has_next":true,"next_page":3,"next_row":2},"results":[{"id":2,"nucleus_url":"http:\/\/n2\/delivery_modes\/id\/2","description":"Full Time"}]}''

additional_endpoints:
 - url: /id/
   description: Returns a specific record for a delivery mode, when providing a valid ID.
---

Delivery modes are used to indicate how programmes of study can be delivered at the university, i.e. full-time.
