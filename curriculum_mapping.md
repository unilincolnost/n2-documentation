---
layout: object

title: Curriculum Mapping
nav: objects
endpoint: /curriculum_mappings

query_params :
 - name: module_id
   type: string
   description: The ID of a module delivered at the university.
 - name: programme_outcome_id
   type: string
   description: The ID of a programme outcome.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130123","pagination":{"page_size":1,"items_on_page":1,"current_page":2,"current_row":1,"total_rows":27520,"last_row":27520,"total_pages":27520,"has_previous":true,"previous_page":1,"previous_row":0,"has_next":true,"next_page":3,"next_row":2},"results":[{"id":2,"nucleus_url":"http:\/\/n2\/curriculum_mappings\/id\/2","module":{"id":1,"nucleus_url":"http:\/\/n2\/modules\/id\/1","module_code":{"id":1,"nucleus_url":"http:\/\/n2\/module_codes\/id\/1","code":"ENL9020M"},"title":"21st Century Short Stories 2011-12","level":{"id":1,"nucleus_url":"http:\/\/n2\/levels\/id\/1","description":"Masters"}},"programme_outcome":{"id":11,"nucleus_url":"http:\/\/n2\/programme_outcomes\/id\/11","category":{"id":2,"nucleus_url":"http:\/\/n2\/programme_outcome_categories\/id\/2","title":"Subject Specific Intellectual Skills"},"description":"Able to reflect critically on how different social contexts and cultural norms (including the student\u2019s own) affect textual reception and production.","sandwich_variant_only":false},"mode":"Delivered"}]}'

additional_endpoints:
	- url: /id/<id>
	  description: Returns a specific curriculum mapping record.
---