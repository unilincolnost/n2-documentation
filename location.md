---
layout: object

title: Location
nav: objects
endpoint: /locations

query_params :
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130125","pagination":{"page_size":1,"items_on_page":1,"current_page":1,"current_row":0,"total_rows":10,"last_row":10,"total_pages":10,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":true,"next_page":2,"next_row":1},"results":[{"id":1,"uri":"id.lincoln.ac.uk\/location\/1","nucleus_url":"http:\/\/n2\/locations\/id\/1","planon_id":5000652,"estates_code":"BR-000","name":"Brayford Pool","parent_location":null}]}'

additional_endpoints:
 - url: /id/<id>
   description: Returns a specific infrastructure nickname record, when providing a valid ID.
---