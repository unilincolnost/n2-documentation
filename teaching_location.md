---
layout: object

title: Teaching Location
nav: objects
endpoint: /teaching_locations

query_params :
 - name: title
   type: string
   description: The title given to the teaching location.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":2,"num_results":1,"results":[{"id":1,"nucleus_uri":"http:\/\/n2\/teaching_locations\/id\/1","code":"X","name":"Boston College"}]}'

additional_endpoints:
 - url: /id/<id>
   description: Returns a specific teaching location record, when providing a valid ID.
---
