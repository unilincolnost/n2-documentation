---
layout: object

title: Infrastructure
nav: objects
endpoint: /infrastructures

query_params :
 - name: location_id
   type: int
   description: Returns infrastructure at the specified location. Uses the Nucleus ID of the location.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130129","pagination":{"page_size":1,"items_on_page":1,"current_page":1,"current_row":0,"total_rows":43,"last_row":43,"total_pages":43,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":true,"next_page":2,"next_row":1},"results":[{"id":1,"nucleus_url":"https:\/\/n2.online.lincoln.ac.uk\/infrastructures\/id\/1","planon_id":"5000696","estates_code":"BR-013","name":"Brayford N Gas Meter","build_year":"1996","location_id":"3","location":{"id":3,"uri":"id.lincoln.ac.uk\/location\/3","nucleus_url":"https:\/\/n2.online.lincoln.ac.uk\/locations\/id\/3","planon_id":5000965,"estates_code":"BR-089","name":"Infrastructure & Land","parent_location":{"id":1,"uri":"id.lincoln.ac.uk\/location\/1","nucleus_url":"https:\/\/n2.online.lincoln.ac.uk\/locations\/id\/1","planon_id":5000652,"estates_code":"BR-000","name":"Brayford Pool","parent_location":null}}}]}'

additional_endpoints:
 - url: /id/<id>
   description: Returns a specific infrastructure record, when providing a valid ID.
---