---
layout: object

title: Marketing Record
nav: objects
endpoint: /marketing_records

query_params :
 - name: programme_id
   type: int
   description: Returns marketing records associated with the programme ID specified.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":2,"num_results":1,"results":[{"id":1,"nucleus_uri":"http:\/\/n2\/marketing_records\/id\/1","introduction":null}]}'

additional_endpoints:
 - url: /id/
   description: Returns a specific record for a marketing record, when providing a valid ID.
---

Marketing information associated with a particular programme of study.
