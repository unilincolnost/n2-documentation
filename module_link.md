---
layout: object

title: Module Link
nav: objects
endpoint: /module_links

query_params :
 - name: programme_id
   type: int
   description: Returns module links associated with this programme ID.
 - name: module_id
   type: int
   description: Returns module links associated with this module.
 - name: core
   type: bool
   description: Returns module links that are core (true) or optional (false).
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.  Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.   Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130123","pagination":{"page_size":1,"items_on_page":1,"current_page":1,"current_row":0,"total_rows":19653,"last_row":19653,"total_pages":19653,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":true,"next_page":2,"next_row":1},"results":[{"id":1,"nucleus_url":"http:\/\/n2\/module_links\/id\/1","core":false,"programme":{"id":1,"nucleus_url":"http:\/\/n2\/programmes\/id\/1","programme_title":"21st Century Literature","course_title":"21st Century Literature Master of Arts (MA) 2011-12","url":null,"academic_year":{"id":1,"nucleus_url":"http:\/\/n2\/academic_years\/id\/1","name":"2011-12"},"institution":{"id":1,"uri":"id.lincoln.ac.uk\/instituion\/1","nucleus_url":"http:\/\/n2\/institutions\/id\/1","name":"University of Lincoln"}},"module":{"id":1,"nucleus_url":"http:\/\/n2\/modules\/id\/1","module_code":{"id":1,"nucleus_url":"http:\/\/n2\/module_codes\/id\/1","code":"ENL9020M"},"title":"21st Century Short Stories 2011-12","level":{"id":1,"nucleus_url":"http:\/\/n2\/levels\/id\/1","description":"Masters"}},"programme_id":"1"}]}'

additional_endpoints:
 - url: /id/
   description: Returns a specific record for a module link, when providing a valid ID.
---

Module links are used to indicate the links between modules and programmes. A module is linked to a programme when it is taught as part of that programme.
