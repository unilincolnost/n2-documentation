---
layout: object

title: Learning Outcome
nav: objects
endpoint: /learning_outcomes

query_params :
 - name: description
   type: string
   description: Returns learning outcomes that have a description 'like' the string specified.
 - name: reference
   type: string
   description: Returns learning outcomes that have a reference 'like' the string specified.
 - name: module_id
   type: int
   description: Returns learning outcomes associated with the specified module.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130123","pagination":{"page_size":1,"items_on_page":1,"current_page":1,"current_row":0,"total_rows":38536,"last_row":38536,"total_pages":38536,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":true,"next_page":2,"next_row":1},"results":[{"id":1,"nucleus_url":"http:\/\/n2\/learning_outcomes\/id\/1","description":"Demonstrate awareness of historical and ongoing debates about short story genre and definition.","reference":"LO1"}]}'

additional_endpoints:
 - url: /id/
   description: Returns a specific record for a learning outcome, when providing a valid ID.
---

Learning outcomes describe what a student should know, understand, or be able to do at the end of that module.