---
layout: object

title: People Location
nav: objects
endpoint: /people_locations

query_params :
 - name: account_id
   type: int
   description: Returns records relevant to the specifed user account ID.
 - name: space_id
   type: int
   description: Returns records relevant to the specifed space ID.
 - name: building_id
   type: int
   description: Returns records relevant to the specifed building ID.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.  Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.   Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":2,"num_results":1,"results":[{"id":1,"planon_id":"6116","account_id":"001874","building_id":"1","space_id":"2126"}]}'

additional_endpoints:
 - url: /id/<id>
   description: Returns a specific infrastructure nickname record, when providing a valid ID.
---
