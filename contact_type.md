---
layout: object

title: Contact Type
nav: objects
endpoint: /contact_types

query_params :
 - name: title
   type: string
   description: The title given to a type of contact time i.e. lecture.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130123","pagination":{"page_size":1,"items_on_page":1,"current_page":2,"current_row":1,"total_rows":15,"last_row":15,"total_pages":15,"has_previous":true,"previous_page":1,"previous_row":0,"has_next":true,"next_page":3,"next_row":2},"results":[{"id":2,"nucleus_url":"http:\/\/n2\/contact_types\/id\/2","title":"Clinical Placement","contact_type_category":{"id":2,"nucleus_url":"http:\/\/n2\/contact_type_categories\/id\/2","title":"Placement"}}]}'

additional_endpoints:
 - url: /id/<id>
   description: Returns a specific contact type record, when providing a valid ID.
---

Contact time at the university is broken down into types, such as 'lecture'.