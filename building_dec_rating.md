---
layout: object

title: Building DEC Rating
nav: objects
endpoint: /building_dec_ratings

query_params :
 - name: building_id
   type: int
   description: The Nucleus ID of the building to which the components belong.
 - name: year_id
   type: int
   description: The Nucleus ID of a year.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130129","pagination":{"page_size":1,"items_on_page":1,"current_page":1,"current_row":0,"total_rows":68,"last_row":68,"total_pages":68,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":true,"next_page":2,"next_row":1},"results":[{"id":31,"nucleus_url":"https:\/\/n2.online.lincoln.ac.uk\/building_dec_ratings\/id\/31","building":{"id":8,"uri":"id.lincoln.ac.uk\/building\/8","nucleus_url":"https:\/\/n2.online.lincoln.ac.uk\/buildings\/id\/8","osm_way_id":82810768,"planon_id":5000722,"estates_code":"BR-009","name":"Art, Architecture & Design Building","build_year":2003,"latitude":53.227832,"longitude":-0.548357,"maki_icon":null},"year":{"id":9,"nucleus_url":"https:\/\/n2.online.lincoln.ac.uk\/years\/id","year":"2012"},"rating":"87"}]}'

additional_endpoints:
 - url: /id/<id>
   description: Returns a specific record for a building dec rating, when providing a valid ID.
---
