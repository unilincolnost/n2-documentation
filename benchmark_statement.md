---
layout: object

title: Benchmark Statement
nav: objects
endpoint: /benchmark_statements

query_params :
 - name: statement
   type: string
   description: The textual description of the benchmark statement.
 - name: level_id
   type: int
   description: The ID of benchmark statement level associated with benchmark statements.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json : '{"error":false,"http_status_code":200,"nucleus_version":"2.20130123","pagination":{"page_size":1,"items_on_page":1,"current_page":2,"current_row":1,"total_rows":458,"last_row":458,"total_pages":458,"has_previous":true,"previous_page":1,"previous_row":0,"has_next":true,"next_page":3,"next_row":2},"results":[{"id":3,"nucleus_url":"http:\/\/n2\/benchmark_statements\/id\/3","reference":"Eng1","benchmark_statement_level":{"id":3,"nucleus_url":"http:\/\/n2\/benchmark_statement_levels\/id\/3","level":"Threshold"},"statement":"This is the minimum requirement that should be reached by honours graduates.","status":""}]}'

additional_endpoints:
 - url: /id/<id>
   description: Returns a specific record for a benchmark statement, when providing a valid ID.
---
