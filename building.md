---
layout: object

title: Building
nav: objects
endpoint: /buildings

query_params :
 - name: estates_code
   type: string
   description: The code used to identify the building, i.e. BR-001
 - name: location_id
   type: int
   description: The Nucleus ID of a building's parent location.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130129","pagination":{"page_size":1,"items_on_page":1,"current_page":1,"current_row":0,"total_rows":132,"last_row":132,"total_pages":132,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":true,"next_page":2,"next_row":1},"results":[{"id":1,"uri":"id.lincoln.ac.uk\/building\/1","nucleus_url":"https:\/\/n2.online.lincoln.ac.uk\/buildings\/id\/1","osm_way_id":27033097,"planon_id":5000718,"estates_code":"BR-001","name":"Main Admin Building","build_year":1996,"latitude":53.228467,"longitude":-0.547878,"maki_icon":null}]}'

additional_endpoints:
 - url: /id/<id>
   description: Returns a specific record for a building, when providing a valid ID.
---
