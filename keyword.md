---
layout: object

title: Keyword
nav: objects
endpoint: /keywords

query_params :
 - name: term
   type: string
   description: Returns keywords 'like' the term specified.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130123","pagination":{"page_size":1,"items_on_page":1,"current_page":1,"current_row":0,"total_rows":9788,"last_row":9788,"total_pages":9788,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":true,"next_page":2,"next_row":1},"results":[{"id":1,"nucleus_url":"http:\/\/n2\/keywords\/id\/1","keyword":"Education"}]}'

additional_endpoints:
 - url: /id/
   description: Returns a specific record for a keyword, when providing a valid ID.
---

Identified keywords from programme data.