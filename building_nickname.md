---
layout: object

title: Building Nickname
nav: objects
endpoint: /building_nicknames

query_params :
 - name: building_id
   type: int
   description: The Nucleus ID of the building to which the nicknames relate.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130129","pagination":{"page_size":1,"items_on_page":1,"current_page":1,"current_row":0,"total_rows":38,"last_row":38,"total_pages":38,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":true,"next_page":2,"next_row":1},"results":[{"id":1,"nucleus_url":"https:\/\/n2.online.lincoln.ac.uk\/building_nicknames\/id\/1","nickname":"Main Academic Building (MAB)"}]}'

additional_endpoints:
 - url: /id/<id>
   description: Returns a specific record for a building nickname, when providing a valid ID.
---
