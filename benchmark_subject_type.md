---
layout: object

title: Benchmark Subject Types
nav: objects
endpoint: /benchmark_subject_types

query_params :
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json : '{"error":false,"http_status_code":200,"nucleus_version":"2.20130123","pagination":{"page_size":1,"items_on_page":1,"current_page":2,"current_row":1,"total_rows":2,"last_row":2,"total_pages":2,"has_previous":true,"previous_page":1,"previous_row":0,"has_next":false,"next_page":2,"next_row":2},"results":[{"id":2,"nucleus_url":"http:\/\/n2\/benchmark_subject_types\/id\/2","title":"QAA"}]}'

additional_endpoints:
 - url: /id/<id>
   description: Returns a specific record for a benchmark subject type, when providing a valid ID.
---