---
layout: object_wrapper

title: Objects
nav: objects
---

Nucleus is built around the concept of objects. An object represents a distinct thing within the University, such as a person, module, assessment or location.

An object may include other objects within it. Exactly which objects are included with another and in what depth of detail depends upon the object type being viewed. For example, a module object will include a high-level overview of all the programmes which contain that module, whereas for additional details you will have to view the programme objects directly.