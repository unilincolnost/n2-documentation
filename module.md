---
layout: object

title: Module
nav: objects
endpoint: /modules

query_params :
 - name: code
   type: string
   description: The code used to identify the module.
 - name: title
   type: string
   description: The title given to a module.
 - name: academic_year_id
   type: int
   description: The ID of the academic year to which a module is associated.
 - name: school_id
   type: int
   description: The ID of the school that is responsible for the module.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.  Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.   Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130123","pagination":{"page_size":1,"items_on_page":1,"current_page":1,"current_row":0,"total_rows":7053,"last_row":7053,"total_pages":7053,"has_previous":false,"previous_page":1,"previous_row":0,"has_next":true,"next_page":2,"next_row":1},"results":[{"id":1,"nucleus_url":"http:\/\/n2\/modules\/id\/1","module_code":{"id":1,"nucleus_url":"http:\/\/n2\/module_codes\/id\/1","code":"ENL9020M"},"title":"21st Century Short Stories 2011-12","level":{"id":1,"nucleus_url":"http:\/\/n2\/levels\/id\/1","description":"Masters"},"learning_outcomes":[{"id":1,"nucleus_url":"http:\/\/n2\/learning_outcomes\/id\/1","description":"Demonstrate awareness of historical and ongoing debates about short story genre and definition.","reference":"LO1"},{"id":2,"nucleus_url":"http:\/\/n2\/learning_outcomes\/id\/2","description":"Analyse a range of contemporary short stories in relation to theoretical contexts.","reference":"LO2"},{"id":3,"nucleus_url":"http:\/\/n2\/learning_outcomes\/id\/3","description":"Understand aspects of formal and thematic variation and experimentation in the short story.","reference":"LO3"},{"id":4,"nucleus_url":"http:\/\/n2\/learning_outcomes\/id\/4","description":"Relate the contemporary short story to some earlier cultural and literary contexts.","reference":"LO4"},{"id":5,"nucleus_url":"http:\/\/n2\/learning_outcomes\/id\/5","description":"Demonstrate the ability to identify a research question, and plan and implement a small-scale research project.","reference":"LO5"},{"id":6,"nucleus_url":"http:\/\/n2\/learning_outcomes\/id\/6","description":"Present research in the format of a 20 minute paper in a small internal postgraduate symposium.","reference":"LO6"}]}]}'

additional_endpoints:
 - url: /id/
   description: Returns a specific record for a module, when providing a valid ID.
---

Modules are units of study.
