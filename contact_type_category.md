---
layout: object

title: Contact Type Category
nav: objects
endpoint: /contact_type_categories

query_params :
 - name: title
   type: string
   description: The title given to a category of types of contact time.
 - name: limit
   type: int
   description: Allows you to limit the amount of results returned per page.	Default 100.
 - name: page
   type: int
   description: Allows you to set which page of results to return.	Default 1. If page number specified is greater than amount of pages, will return last page of results.

expected_output_json: '{"error":false,"http_status_code":200,"nucleus_version":"2.20130123","pagination":{"page_size":1,"items_on_page":1,"current_page":2,"current_row":1,"total_rows":3,"last_row":3,"total_pages":3,"has_previous":true,"previous_page":1,"previous_row":0,"has_next":true,"next_page":3,"next_row":2},"results":[{"id":2,"nucleus_url":"http:\/\/n2\/contact_type_categories\/id\/2","title":"Placement"}]}'

additional_endpoints:
 - url: /id/<id>
   description: Returns a specific record for a contact type category, when providing a valid ID.
---

Contact time at the university is broken down into types, such as 'lecture'. These types are then futher categorised into contact type categories.